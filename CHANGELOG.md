# Changelog for the Micro-CI Package

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.2] - 2021-02-02

### Changed

- deploy directly from `main` branch

## [0.1.1] - 2021-02-01

### Fixed

- exclude deploy stage from unittest run

## [0.1.0] - 2021-02-01

### Added

- initial version of `mci`

## [0.0.0] - 2021-02-01

### Added

- package basics
