# Copyright (c) 2020-2021, Gauss Machine Learning GmbH. All rights reserved.
# This file is part of the TEMPLATE. Please do not distribute.

# type: ignore
# pylint: disable=missing-module-docstring

import micro_ci


def test_import():
    """Check whether the import worked by printing the version information."""
    print(micro_ci.__version__)


def test_main():
    """Just run the main function."""
    micro_ci.main(disable=["test", "cov", "deploy"])
